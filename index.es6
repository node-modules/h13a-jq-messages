const packageJson = require("./package.json");
const packageJsonMain = require("../../package.json");

const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g,'');
const widgetName = packageJson.name.toLowerCase().replace(/-/g,'');
const widgetFullName = `${widgetNamespace}.${widgetName}`;

require("h13a-strategy3"); // Simple 1+2 Strategy, initialize and then display ... display ... display
const widgetIntegrationStrategy = $[widgetNamespace].h13astrategy3;

const defaults = require("./defaults.es6");

const functions = {

    _rnd: function(min, max){
      return Math.floor( Math.random() * (max - parseInt(min) + 1)) + parseInt(min);
    },

    _interpolator: function( ){
      let response = "";
      const textGenerator   = () => this.option('messages')[this._rnd(0, this.option('messages').length - 1)];
      const typeGenerator   = () => this.option('classes')[this._rnd(0, this.option('classes').length - 1)];
      const numberGenerator = () => this.option('numbers')[this._rnd(0, this.option('numbers').length - 1)];
      response = this.option('template')
        .replace(/\$TEXT/g, textGenerator)
        .replace(/\$TYPE/g, typeGenerator)
        .replace(/\$NUMBER/g, numberGenerator);
      return response;
    },

    // <<YOUR FUNCTIONS HERE>>

}

const integration = $.extend({
  options: defaults,
  _destroy: function(){
    // The public destroy() method cleans up all common data, events, etc. and then delegates out to _destroy() for custom, widget-specific, cleanup.
    console.log(`${this.widgetFullName}/${this.uuid}: destructor executed.`)
  },

  // Step1 Notes
  // * Create UI
  // * Called once at start
  // * Call display manually
  _prepare: function () {
    const textUpdateInterval = setInterval(()=>{
      if( this._rnd(1, this.option('poolsize')) == 1 ) this.display();
    }, this.option('interval'));

    this.destructors(()=>clearInterval(textUpdateInterval));

    // manual call
    this.display();
  },

  // Step2 Notes
  // * Display On Screen
  // * This will be called each time options finish changing
  // * Called after _prepare finishes
  // * Called each time options change.
  display: function () {
    // Call Helper
    this.contentNode.html( this._interpolator() );
  },
}, functions);

$.widget(widgetFullName, widgetIntegrationStrategy, integration);
