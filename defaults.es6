let somethingThatIsCalculated = 2 + 2;
module.exports = {

  poolsize: 4,
  interval: 1000,
  template: '<div class="m-y-1 alert alert-$TYPE" role="alert"> <strong>$TEXT</strong></div>',
  messages: ["Marco!", "Polo!"],
  classes: [ 'none', 'info', 'success', 'danger', 'warning' ],
  numbers: [ 3, 7, 13, 31, 37, 43, 67, 73, 79, 127, 151, 163, 193, 211, 223, 241, 283, 307, 331 ],

  // <<PUT YOUR SETTINGS HERE>>

};
